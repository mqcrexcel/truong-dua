/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LCD_H
#define __LCD_H

#ifdef __cplusplus
 extern "C" {
#endif 

/*
*************************************************************************************************************************************
*															INCLUDED FILES															*
*************************************************************************************************************************************
*/
#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_gpio.h"
#include "stm32f1xx_hal_rcc.h"

#define RS_Pin GPIO_PIN_0
#define RS_GPIO_Port GPIOB
#define EN_Pin GPIO_PIN_1
#define EN_GPIO_Port GPIOB
#define D4_Pin GPIO_PIN_2
#define D4_GPIO_Port GPIOB
#define D5_Pin GPIO_PIN_10
#define D5_GPIO_Port GPIOB
#define D6_Pin GPIO_PIN_11
#define D6_GPIO_Port GPIOB
#define D7_Pin GPIO_PIN_12
#define D7_GPIO_Port GPIOB

void LCD_Enable();
void LCD_Send4Bit(unsigned char Data);
void LCD_SendCommand(unsigned char command);
void LCD_Clear();
void LCD_Init();
void LCD_Gotoxy(unsigned char x, unsigned char y);
void LCD_PutChar(unsigned char Data);
void LCD_Puts(char *s);



#ifdef __cplusplus
}
#endif


#endif /* __LCD_H */


